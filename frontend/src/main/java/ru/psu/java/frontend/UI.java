package ru.psu.java.frontend;

import ru.psu.java.common.Pet;
import ru.psu.java.common.PetDao;

import java.time.LocalDate;

public class UI {
    private final PetDao petDao;

    public UI(PetDao petDao) {
        this.petDao = petDao;
    }

    public void start() {
        Pet mystic = new Pet("Mystic", LocalDate.of(2020, 1, 24));
        Pet bella = new Pet("Bella", LocalDate.of(2010, 1, 1));
        petDao.save(mystic);
        petDao.save(bella);
        petDao.getAll().map(Pet::prettyString)
                .forEach(System.out::println);
    }
}
