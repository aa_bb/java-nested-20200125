package ru.psu.java;

import ru.psu.java.backend.PetDaoLocal;
import ru.psu.java.common.PetDao;
import ru.psu.java.frontend.UI;

public class app {
    public static void main(String[] args) {
        PetDao petDao = new PetDaoLocal();
        UI ui = new UI(petDao);
        ui.start();
    }
}
